public class AdapterLightningToJack implements Lightning {
    private final JackCable jackCable;

    public AdapterLightningToJack(JackCable jackCable){
        this.jackCable = jackCable;
    }

    @Override
    public void connectWithLightning() {
        this.jackCable.insert();
        this.jackCable.playMusic();
    }
}
